package com.food.order.bean;

import com.food.order.db.MenuItem;
import com.food.order.db.MenuOfWeek;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author safi
 */
@Stateless
@LocalBean
public class Menu {

    @PersistenceContext
    private EntityManager em;

    public List<MenuItem> findMenuOfWeek(int week) {
        List<MenuItem> menuItemOfWeek;
        List<MenuOfWeek> result = em.createNamedQuery("MenuOfWeek.findByWeek")
                .setParameter("week", week)
                .setHint("javax.persistence.fetchgraph", em.getEntityGraph("menuWithItems"))
                .getResultList();

        menuItemOfWeek = result.get(0).getMenuItems();
        return menuItemOfWeek;
    }

    public List<MenuOfWeek> findAllMenuesOfWeek() {
        return em.createNamedQuery("MenuOfWeek.findAll")
                .setHint("javax.persistence.fetchgraph", em.getEntityGraph("menuWithItems"))
                .getResultList();
    }
}
