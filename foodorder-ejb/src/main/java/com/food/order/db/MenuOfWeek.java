package com.food.order.db;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This is a simple example of a entity class.
 *
 * @author safi
 */
@Entity
@Table(name = "MENU_OF_WEEK")
@NamedQueries({
  @NamedQuery(name = "MenuOfWeek.findAll", query = "SELECT m FROM MenuOfWeek m"),
  @NamedQuery(name = "MenuOfWeek.findByWeek", query = "SELECT m FROM MenuOfWeek m WHERE m.week = :week"),
  @NamedQuery(name = "MenuOfWeek.findByWeeks", query = "SELECT m FROM MenuOfWeek m WHERE m.week IN :weeks")
})
@NamedEntityGraphs({
  @NamedEntityGraph(
          name = "menuWithItems",
          attributeNodes = {
            @NamedAttributeNode("menuItems")
          })
})
public class MenuOfWeek implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  private int year;

  @Column
  private int week;

  @OneToMany(mappedBy = "menuOfWeek")
  private List<MenuItem> menuItems;

  public MenuOfWeek() {
  }

  public MenuOfWeek(Long id, int year, int week, List<MenuItem> menuItems) {
    this.id = id;
    this.year = year;
    this.week = week;
    this.menuItems = menuItems;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<MenuItem> getMenuItems() {
    return menuItems;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getYear() {
    return year;
  }

  public void setWeek(int week) {
    this.week = week;
  }

  public int getWeek() {
    return week;
  }

}
