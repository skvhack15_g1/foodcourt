package com.food.order.db;

import java.io.Serializable;
import java.time.DayOfWeek;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author safi
 */
@Entity
public class MenuItem implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  
  @Column
  private DayOfWeek day;
  
  @Column
  private String presentation;
  
  @ManyToOne
  private MenuOfWeek menuOfWeek;

  public MenuItem() {
  }

  public MenuItem(Long id, DayOfWeek day, MenuOfWeek menuOfWeek, String presentation) {
    this.id = id;
    this.day = day;
    this.menuOfWeek = menuOfWeek;
    this.presentation = presentation;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public DayOfWeek getDay() {
    return day;
  }

  public void setDay(DayOfWeek day) {
    this.day = day;
  }

  public MenuOfWeek getMenuOfWeek() {
    return menuOfWeek;
  }

  public void setMenuOfWeek(MenuOfWeek menuOfWeek) {
    this.menuOfWeek = menuOfWeek;
  }

  public String getPresentation() {
    return presentation;
  }

  public void setPresentation(String presentation) {
    this.presentation = presentation;
  }
  
}
