package com.food.order.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author safi
 */
@ApplicationPath("resource")
public class MyApplication extends Application {
  
}
