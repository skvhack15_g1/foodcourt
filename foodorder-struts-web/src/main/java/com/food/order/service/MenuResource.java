package com.food.order.service;

import com.food.order.bean.Menu;
import com.food.order.db.MenuItem;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author safi
 */
@Path("/menu")
public class MenuResource {

  @EJB
  private Menu menuBean;

  public MenuResource() throws NamingException {
    
    if (menuBean == null) {
      menuBean = (Menu) new InitialContext().lookup("java:global/Enterprise_Application_module/foodorder-ejb-1.0-SNAPSHOT/Menu");
    }
    
  }

  @GET
  @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
  public List<MenuItem> getMenuOfWeek() throws Exception {
    int weekNo = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
    return menuBean.findMenuOfWeek(weekNo);   
  }

  @GET
  @Path("/{weekno}")
  @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
  public List<MenuItem> getMenuOfWeek(@PathParam("weekno") int weekNo) throws Exception {
    
    return menuBean.findMenuOfWeek(weekNo);
    
  }
  
}
