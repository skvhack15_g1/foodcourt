package com.food.order.action;

import com.opensymphony.xwork2.ActionSupport;

import com.food.order.bean.Menu;
import com.food.order.db.MenuItem;
import java.util.Set;
import javax.annotation.Resource;
import javax.naming.InitialContext;

/**
 *
 * @author safi
 */
public class MenuAction extends ActionSupport {

  private int weekNumber;
  private Set<MenuItem> menuItems;

  @Resource(lookup = "java:global/Enterprise_Application_module/foodorder-ejb-1.0-SNAPSHOT/Menu")
  private Menu menuBean;

  public String doList() throws Exception {
    if (menuBean == null) {
      menuBean = (Menu) new InitialContext().lookup("java:global/Enterprise_Application_module/foodorder-ejb-1.0-SNAPSHOT/Menu");
    }
    //menuItems = menuBean.findMenuOfWeek(weekNumber).get(0);

    return SUCCESS;
  }

  public int getWeekNumber() {
    return weekNumber;
  }

  public void setWeekNumber(int weekNumber) {
    this.weekNumber = weekNumber;
  }

  public Set<MenuItem> getMenuItems() {
    return menuItems;
  }

}
