(function() {
    require.config({
        baseUrl: 'js/lib',
        paths: {
            vendor: '../vendor',
            jquery: '../vendor/jquery.min',
            underscore: '../vendor/underscore-min',
            backbone: '../vendor/backbone-min',
        }
    });

    require(['app'], function(app) {
        app.run({baseUrl: 'http://localhost:8080/foodorder-struts-web/'});
    });
})();
