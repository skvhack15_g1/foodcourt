define(['jquery', 'underscore', 'backbone', 'collections/menu', 'views/menu'], function($, _, Backbone, Menu, MenuView) {
    var app = {};
    
    
    
    app.run = function(opts) {
        opts = opts || {};
        window.appConfig = {};
        window.appConfig.baseUrl = opts.baseUrl;
        
        var m = new Menu();
        m.fetch({
            success: function(col) {
                new MenuView({collection: col}).render();
                
            }
        });
        
        
    };

    return app;
});
