define(['backbone'], function(Backbone) {
    var Food = Backbone.Model.extend({
        defaults: {
            day: '',
            presentation: ''
        }
    });
    
    return Food;
});