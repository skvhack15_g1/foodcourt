define(['underscore', 'backbone'], function(_, Backbone) {
    var MenuView = Backbone.View.extend({
        el: '#AppMain',
        
        initialize: function() {
            this.listenTo(this.collection, 'add', this.addOne);  
        },
        
        render: function() {
            //var documents = [new Backbone.Model({day:'Monday', presentations:'Ärtsoppa'})];
            console.log(this.collection);
            var template = _.template($('#MenuList').html());
            this.$el.html(template({col: this.collection.toJSON()}));
        }
    });
    
    return MenuView;
});