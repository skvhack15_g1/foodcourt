define(['backbone', 'models/food'], function(Backbone, Food) {
    var Menu = Backbone.Collection.extend({
        url: 'http://localhost:8080/foodorder-struts-web/' + 'resource/menu',
        model: Food
    });
    
    return Menu;
});