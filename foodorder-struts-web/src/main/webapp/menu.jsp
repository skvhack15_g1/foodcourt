<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Veckans meny</title>
  </head>
  <body>
    <h1>Veckans meny</h1>

    <p>
      <s:iterator value="menuItems" status="status">
    <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>"><s:property value="day"/> <s:property value="presentation"/></tr>
    </s:iterator>
  </p>

</body>
</html>