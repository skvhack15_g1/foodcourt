<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
  <head>
    <style>
    #headerdiv {       
        border: 0px dotted #787878;
        padding-left: 10px;
        
    }
    #main {
        color: #787878;
        position:relative;
        left:120px;
         
    }
    h1 {
        color: #787878;
        font-size: 25px;
        position:relative;
        left:120px;
        top: -80px;
    }
    </style>
    <meta charset="UTF-8">
    <title>Välj veckomeny</title>
  </head>
  <body>
      <div id="headerdiv">  
          <img src="matbild.jpg" alt="Matbild" height="100" width="100">
          <h1> SKV_IT Visbys magiska lunchbeställningsapp</h1>        
      </div>  
      <div id="main">
          <s:form action="menu">
            <s:textfield name="weekNumber" label="Välj veckonr" />
            <s:submit value="Submit" />
          </s:form>
      </div>
  </body>
</html>